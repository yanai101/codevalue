import React from 'react';
import Header from './components/layoutComponents/header'
import style from './app.module.scss';
import {  BrowserRouter as Router , Route, Switch } from 'react-router-dom';
import Notfound from './pages/notfound/Notfound';
import { createBrowserHistory } from "history";
import HomePage from './pages/home/homePage';
import Categories from './pages/categories/categories';


const history = createBrowserHistory(); 


function App() {
  return (
    <main className={style.AppContainer}>
      <Router history={history}>
        <Header/>  
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/categories" component={Categories} />
          <Route component={Notfound} />
        </Switch>
      </Router >  
    </main>
  );
}

export default App;
