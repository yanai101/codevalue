const STORAGE = 'categories';

export function getCategories(){
    return JSON.parse(localStorage.getItem(STORAGE)) || {}; 
}

export function getCategory(categoryId){
    const categories = getCategories();
    return categories[categoryId];
}

export function addCategory(categoryName){
    const { categoriesArray, categories } = prepareCategories();
    if(!categoriesArray.includes(categoryName)){
        categories[Date.now()] = categoryName;
        localStorage.setItem(STORAGE,  JSON.stringify(categories));
    }
    return categories;
}

export function updateCategory(categoryId , categoryName){
    const {categories } = prepareCategories();
    categories[categoryId] = categoryName;
    localStorage.setItem(STORAGE,  JSON.stringify(categories));
    return categories;
}

export function deleteCategory(categoryId){
    const {categories } = prepareCategories();
    delete categories[categoryId];
    localStorage.setItem(STORAGE,  JSON.stringify(categories));
    return categories;
}

function prepareCategories() {
    const categories = getCategories();
    const categoriesArray = Object.values(categories);
    return { categoriesArray, categories };
}