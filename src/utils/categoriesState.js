import React from 'react';
import {addCategory , deleteCategory , getCategories ,updateCategory , getCategory} from './apiManager';
import { CATEGORY_ACTIONS } from '../config/config';


function categoriesReducer(state, action) {
    switch (action.type) {
        case CATEGORY_ACTIONS.GET: {
            return  action.categoryId ? getCategory(action.categoryId) : getCategories();
        }
        case CATEGORY_ACTIONS.ADD: {
            return addCategory(action.categoryName);  
        }
        case CATEGORY_ACTIONS.DELETE: {
            return deleteCategory(action.categoryId)  
        }
        case CATEGORY_ACTIONS.UPDATE: {
            return updateCategory(action.categoryId, action.categoryName)  
        }
        default: {
            return ;
        }
    }
}


export function useCategories() {
    return  React.useReducer(categoriesReducer,getCategories());
}

