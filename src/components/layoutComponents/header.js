import React from "react";
import { NavLink}  from "react-router-dom";
import style from './header.module.scss';
import {MdFormatListBulleted, MdHome} from 'react-icons/md';

export default function App() {
  return ( 
    <nav className={style.headerNav}>
            <NavLink to="/"><MdHome/> Home</NavLink >
            <NavLink to="/categories"><MdFormatListBulleted/> Categories</NavLink >
    </nav>
  );
}
