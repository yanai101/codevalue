import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import {MdDeleteForever , MdLibraryAdd , MdEdit ,MdPageview } from 'react-icons/md';
import './categoryHeader.scss';
import CategoryActionModal from '../catrgoryActionModal/categoryActionModal';
import { CATEGORY_ACTIONS } from '../../../config/config';


export default function CategoryHeader({categories ,dispatch ,activeCategory}){
    const [showModal, setShowModal] = useState(false);
    const [action, setAction] = useState(false);
    const [actionData, setActionData] = useState(null);

    function toggleModal(){
        setShowModal(!showModal);
    }

    function handelAction(actionType){
        setAction(actionType);
        setActionData({id: activeCategory, name: categories[activeCategory]})
        toggleModal();
    }

    return(
        <>
            <ButtonGroup aria-label="category action">
                <Button variant="info" onClick={()=>handelAction(CATEGORY_ACTIONS.ADD)}><MdLibraryAdd/></Button>
                {
                    Object.keys(categories).length > 0 && 
                    <>
                        <Button variant="info" onClick={()=>handelAction(CATEGORY_ACTIONS.DETAILS)} disabled={!activeCategory}><MdPageview/></Button>
                        <Button variant="info" onClick={()=>handelAction(CATEGORY_ACTIONS.UPDATE)} disabled={!activeCategory}><MdEdit/></Button>
                        <Button variant="danger" onClick={()=>handelAction(CATEGORY_ACTIONS.DELETE)} disabled={!activeCategory}><MdDeleteForever/></Button>
                    </>
                }
            </ButtonGroup>
            <CategoryActionModal show={showModal} dispatch={dispatch} toggleModal={toggleModal} type={action} data={actionData}/>
        </>
    )
}
