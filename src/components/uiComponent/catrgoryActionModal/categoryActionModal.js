import React, {useRef } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import './categoryActionModal.scss'
import { CATEGORY_ACTIONS } from '../../../config/config';


export default function CategoryActionModal({show, dispatch , type, toggleModal ,data}) {

  const categoryInput = useRef(null);

  function resetFields(){
    categoryInput.current.value = '';
  }

  function handleAction(){
    switch (type) {
      case CATEGORY_ACTIONS.ADD:
        dispatch({type: CATEGORY_ACTIONS.ADD , categoryName: categoryInput.current.value});
        toggleModal();
        break;
      case CATEGORY_ACTIONS.DELETE:
          dispatch({type:CATEGORY_ACTIONS.DELETE, categoryId: data.id});
          toggleModal();
        break;
      case CATEGORY_ACTIONS.UPDATE:
            dispatch({type:CATEGORY_ACTIONS.UPDATE, categoryId: data.id , categoryName: categoryInput.current.value });
            toggleModal();
          break;  
      default:
        return;
    }
  }

  function display(){
        return <p>
            <label htmlFor="category">category name</label>
            {type === CATEGORY_ACTIONS.DELETE ?
              <strong>Delete {data.name} Category?</strong> : 
              <input name="category" id="category" ref={categoryInput} placeholder={data.name} disabled={ type === CATEGORY_ACTIONS.DETAILS}/>}
          </p>
  }

    return (
      <>  
        <Modal show={show} onHide={resetFields}>
          <Modal.Header>
            <Modal.Title>{type} Category</Modal.Title>
          </Modal.Header>
          <Modal.Body className='formBuilderModal'>
            {data && display()}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={toggleModal}>
              Close
            </Button>
            <Button variant="primary" onClick={handleAction}>
              {type}
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
