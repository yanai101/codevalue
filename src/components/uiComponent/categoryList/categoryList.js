import React, { useState } from 'react';
import { ListGroup} from 'react-bootstrap';
import { Form } from 'react-bootstrap';

export default function CategoryList({categories , setActiveCategory , active}){
    const [listView ,setListView] = useState(false);

    function handelSelect(e){
        setActiveCategory(e.target.value)
    }

    function handleChange(value){
        setListView(value.target.checked)
    }

    return(
        <div>
        <Form.Group id="formGridCheckbox" onChange={handleChange}>
            <Form.Check type="checkbox" label="list View" />
        </Form.Group>

        {
            listView ? 
            <ListGroup as="ul">
                    {
                        Object.keys(categories).map(categoryKey=>(
                            <ListGroup.Item as="li"
                                active={categoryKey && categoryKey=== active}
                                key={categoryKey} 
                                onClick={()=>setActiveCategory(categoryKey)}>
                                {categories[categoryKey]}
                            </ListGroup.Item>
                        ))
                    }
            </ListGroup>
        :
        <Form>
            <Form.Group controlId="exampleForm.ControlSelect1">
                    <Form.Label>Category select</Form.Label>
                    <Form.Control as="select" onChange={handelSelect} value={active || '' }>
                    <option>select</option>
                    {
                        Object.keys(categories).map(categoryKey=>(
                            <option key={categoryKey} value={categoryKey}>{categories[categoryKey]}</option>
                        ))
                    }
                    </Form.Control>
            </Form.Group>
        </Form>
        }
        </div>
    )
}
