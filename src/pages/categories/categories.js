import React, { useEffect, useState } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import { useCategories } from '../../utils/categoriesState';
import CategoryList from '../../components/uiComponent/categoryList/categoryList';
import CategoryHeader from '../../components/uiComponent/categoryHeader/categoryHeader';



export default function Categories(){
    const [categories , dispatch]= useCategories();
    const [activeCategory, setActiveCategory] = useState(null);
    const [loading ,setLoading] = useState(true);

    useEffect(()=>{
        setLoading(false)
    },[])


    return(
        <div> 
            <h2>{loading ? <span><Spinner animation="grow" />loading...</span> :'Categories'}</h2>
            <CategoryHeader categories={categories} dispatch={dispatch} activeCategory={activeCategory}/>
            <CategoryList categories={categories} setActiveCategory={setActiveCategory} active={activeCategory}/>
        </div>
    )
}
