export const CATEGORY_ACTIONS ={
    ADD:'Add',
    DELETE: 'Delete',
    UPDATE: 'Update',
    DETAILS: 'Details',
    GET: 'Get'
}